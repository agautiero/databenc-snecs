﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BottoniInterfaccia : MonoBehaviour {
    public GameObject bottoniPortali;
    public GameObject bottoniChiesa;
    public GameObject bottoniBiblio;
    public GameObject bottoniGeo;
    public GameObject ScrollMarmo;
    public GameObject ScrollPiperno;
    
    // Use this for initialization
    void Start () {
        bottoniPortali.SetActive(false);
        bottoniChiesa.SetActive(false);
        bottoniGeo.SetActive(false);
        bottoniBiblio.SetActive(false);
        ScrollMarmo.SetActive(false);
        ScrollPiperno.SetActive(false);
    }
	public void MostraBottoniPortali(){
       
            bottoniPortali.SetActive(true);
            
        
    }
    public void MostraBottoniChiesa(){
        bottoniChiesa.SetActive(true);
    }
    public void MostraBottoniGeo()
    {
        bottoniGeo.SetActive(true);
    }
    public void MostraBottoniBiblio()
    {
        bottoniBiblio.SetActive(true);
    }
    public void MostraScrollMarmo()
    {
        ScrollMarmo.SetActive(true);
        ScrollPiperno.SetActive(false);
    }
    public void MostraScrollPiperno()
    {
        ScrollMarmo.SetActive(false);
        ScrollPiperno.SetActive(true);
    }
    // Update is called once per frame
    void Update () {
	
	}
}
