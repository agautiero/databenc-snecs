﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Interfaccia : MonoBehaviour {
	public MouseLook cameralook;
	public MouseLook playerlook;

	// Use this for initialization
	void Start () {
	
	}
	public void ClosePopup(){
		gameObject.SetActive (false);

		Cursor.visible = true;
		//Cursor.lockState = CursorLockMode.Locked;
        //DetectColonna.check = 0;
        //DetectColonna.infoCanvasIsActive = false;
        DeserializeInfo.check = 0;
        DeserializeInfo.infoCanvasIsActive = false;
		cameralook.enabled = true; 
		playerlook.enabled = true;

	}

	// Update is called once per frame
	void Update () {
	
	}
}