﻿using UnityEngine;
using System.Collections;

public class Interfaccia : MonoBehaviour {
	public MouseLook cameralook;
	public MouseLook playerlook;
	// Use this for initialization
	void Start () {
	
	}
	public void ClosePopup(){
		gameObject.SetActive (false);

		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		//check = 0;

		cameralook.enabled = true; 
		playerlook.enabled = true;

	}
	// Update is called once per frame
	void Update () {
	
	}
}
