﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using RDFExperiments;
using SimpleJSON;
using MiniJSON;
using System.Collections;

public class DeserializeInfo : MonoBehaviour {
    
    //va settato da unityl'url
    public string url ="";
    //public string url = "http://5.249.129.167:8080/middleware/nodeTreeById?node=_:IT_15_63_F839_San_Marcellino_DTBC_DEGRADO_MATERIALE_NA_FACCIATA_Lato_Chiesa__GEOMATERIALE_PIPERNO_E_TUFO_PIPERNOIDE";
    // Use this for initialization
    // string mydocpath = @"C:\Users\H2iCorsoSAS\Desktop";
    public Text MATERIALE;
    public Text ELEMENTO_DETT;
    public Text ESTENSIONEGEOMAT;
    public Text PERCGEOMAT;
    public Text INTENSITADEGGEOMAT;
    public Text SOMMA_DEGR;
    public Text MATERIALI_DESC;
    public Text MATERIALI_PERC;
    public Text MATERIALI_MQ;

    public AudioClip over;
    public GameObject _info;
    public MouseLook cameralook;
    public MouseLook playerlook;

    public GameObject ScrollMarmo;
    public GameObject ScrollPiperno;
    public GameObject TitoloDescrizioneMateriale;
    public static bool infoCanvasIsActive;

    public static int check = 0;

   
    void Start () {
        _info.SetActive(false);
        MATERIALE.text = "";
        ESTENSIONEGEOMAT.text = "";
        PERCGEOMAT.text = "";
        MATERIALI_DESC.text = "";
        INTENSITADEGGEOMAT.text = "";
        SOMMA_DEGR.text = "";
        MATERIALI_MQ.text = "";
        MATERIALI_PERC.text= "";
        ELEMENTO_DETT.text = "";
        /* try
         {
             //Effettuiamo la chiamata per ottenere la risposta JSON
             //Debug.Log("Url di partenza: "+url );
             HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
             request.Method = "GET";
             request.ContentType = "application/json";

             //Recupero il Subject di interesse, passato nella Request
             var nodeParameter = request.RequestUri.Query.ToString();
             var subjectOfInterest = nodeParameter.Replace("?node=", string.Empty);


             HttpWebResponse response = (HttpWebResponse)request.GetResponse();
             var responseValue = string.Empty;
             if (response.StatusCode == HttpStatusCode.OK)
             {
                 using (var responseStream = response.GetResponseStream())
                 {
                     using (var reader = new StreamReader(responseStream))
                     {
                         responseValue = reader.ReadToEnd();
                         var rdfNodes = new List<RDFData>();

                         var deserializedResponse = JSONNode.Parse(responseValue);
                         var resultRdfJsonNodes = deserializedResponse["result"]; //tutto l'rdf (e SOLO l'RDF!!!!)

                         var allSubjects = (JSONClass) JSON.Parse(resultRdfJsonNodes.ToString()); //parsiamo i nodi dell'RDF iniziale
                         var subjectNode = allSubjects.GetNodesByKey(subjectOfInterest); //recupero solo il soggetto che mi interessa...

                         //for (int i = 0; i < subjectNode.Count; i++)
                         //{
                             //Partendo da ogni soggetto, ci consideriamo i predicati e poi cicliamo ulteriormente
                             //per gli oggetti...

                             var allPredicates = (JSONClass) subjectNode;
                             var predicateNames = allPredicates.GetDictKeys().ToList();

                             for (int j = 0; j < allPredicates.Count; j++)
                             {
                                 //Recupero il nome del predicato j-mo
                                 var predicateName = predicateNames[j];

                                 //Per il predicato j-mo, mi vado a prendere tutti i figli (quindi gli oggetti) 
                                 var allObjects = allPredicates[j].Childs.ToList();

                                 //Ciclo per ogni oggetto e me lo vado ad aggiungere nella lista
                                 for (int k = 0; k < allObjects.Count; k++)
                                 {
                                     var type = allPredicates[j][k]["type"].ToString();
                                     var value = allPredicates[j][k]["value"].ToString();
                                     rdfNodes.Add(new RDFData { Predicate = predicateName, Type = type, Value = value });
                                     Debug.Log("Predicate: " + predicateName + " type : " + type + " value : " + allPredicates[j][k]["value"].ToString());
                                 }

                             }

                         //}

                     }
                 }
             }
         }
         catch (Exception e)
         {
             Debug.Log(e.Message);
         }
         */
    }
   
    public void getDeserializeInfo(string url)
    {
        MATERIALE.text = "";
        ESTENSIONEGEOMAT.text = "";
        PERCGEOMAT.text = "";
        MATERIALI_DESC.text = "";
        INTENSITADEGGEOMAT.text = "";
        SOMMA_DEGR.text = "";
        MATERIALI_MQ.text = "";
        MATERIALI_PERC.text = "";
        ELEMENTO_DETT.text = "";
        var rdfNodes = new List<RDFData>();
        try
        {
            //Effettuiamo la chiamata per ottenere la risposta JSON
            //Debug.Log("Url di partenza: "+url );
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";

            //Recupero il Subject di interesse, passato nella Request
            var nodeParameter = request.RequestUri.Query.ToString();
            var subjectOfInterest = nodeParameter.Replace("?node=", string.Empty);


            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            var responseValue = string.Empty;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                //cambio elemento dettaglio
                if (subjectOfInterest.Contains("GEOMATERIALE_PIPERNO"))//tutti
                {
                    ScrollMarmo.SetActive(false);
                    ScrollPiperno.SetActive(true);
                    TitoloDescrizioneMateriale.SetActive(true);
                    MATERIALE.text = "Piperno";
                    ELEMENTO_DETT.text = "Elementi Architettonici (Archi, Colonne, etc...)";
                }
                else if (subjectOfInterest.Contains("LATO_BIBLIOTECA_GEOMATERIALE_INTONACO"))
                //
                { //tutti tranne chiesa
                    ScrollMarmo.SetActive(false);
                    ScrollPiperno.SetActive(false);
                    TitoloDescrizioneMateriale.SetActive(false);
                    MATERIALE.text = "Intonaco";
                    ELEMENTO_DETT.text = "Elementi Architettonici (Archi, Colonne, etc...)";
                }
                else if (subjectOfInterest.Contains("GEOMATERIALE_RIVESTIMENTO")) { //tutti tranne chiesa
                    ScrollMarmo.SetActive(false);
                    ScrollPiperno.SetActive(false);
                    TitoloDescrizioneMateriale.SetActive(false);
                    MATERIALE.text = "Intonaco";
                    ELEMENTO_DETT.text = "Rivestimento facciata";
                }
                else if (subjectOfInterest.Contains("GEOMATERIALE_CALCARE_E_MARMI")) //portali
                {
                    ScrollMarmo.SetActive(true);
                    ScrollPiperno.SetActive(false);
                    TitoloDescrizioneMateriale.SetActive(true);
                    MATERIALE.text = "Calcare/Marmi";
                    ELEMENTO_DETT.text = "Elementi decorativi";
                }
                else if (subjectOfInterest.Contains("GEOMATERIALE_INTONACO")) //chiesa
                {
                    ScrollMarmo.SetActive(false);
                    ScrollPiperno.SetActive(false);
                    MATERIALE.text = "Intonaco";
                    TitoloDescrizioneMateriale.SetActive(false);
                    ELEMENTO_DETT.text = "Rivestimento facciata";
                }

                using (var responseStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseValue = reader.ReadToEnd();
                        

                        var deserializedResponse = JSONNode.Parse(responseValue);
                        var resultRdfJsonNodes = deserializedResponse["result"]; //tutto l'rdf (e SOLO l'RDF!!!!)

                        var allSubjects = (JSONClass)JSON.Parse(resultRdfJsonNodes.ToString()); //parsiamo i nodi dell'RDF iniziale
                        var subjectNode = allSubjects.GetNodesByKey(subjectOfInterest); //recupero solo il soggetto che mi interessa...

                        //for (int i = 0; i < subjectNode.Count; i++)
                        //{
                        //Partendo da ogni soggetto, ci consideriamo i predicati e poi cicliamo ulteriormente
                        //per gli oggetti...

                        var allPredicates = (JSONClass)subjectNode;
                        var predicateNames = allPredicates.GetDictKeys().ToList();

                        for (int j = 0; j < allPredicates.Count; j++)
                        {
                            //Recupero il nome del predicato j-mo
                            var predicateName = predicateNames[j];
                            
                           // var predicateNameSub = predicateName.Split('#')[1];
                            //Per il predicato j-mo, mi vado a prendere tutti i figli (quindi gli oggetti) 
                            var allObjects = allPredicates[j].Childs.ToList();

                            //Ciclo per ogni oggetto e me lo vado ad aggiungere nella lista
                            for (int k = 0; k < allObjects.Count; k++)
                            {
                                if (predicateName.Contains("ESTENSIONEGEOMAT"))
                                {
                                    ESTENSIONEGEOMAT.text = allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty);
                                }
                                if (predicateName.Contains("PERCGEOMAT"))
                                {
                                    PERCGEOMAT.text = allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty);
                                }
                                if (predicateName.Contains("INTENSITADEGGEOMAT"))
                                {
                                    INTENSITADEGGEOMAT.text = allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty);
                                }
                                if (predicateName.Contains("isReferencedBy"))
                                {
                                    //passo il nodo rdf del materiale di riferimento
                                    degradoInfo(allPredicates[j][k]["value"].ToString());
                                }
                                var type = allPredicates[j][k]["type"].ToString();
                                var value = allPredicates[j][k]["value"].ToString();
                                rdfNodes.Add(new RDFData { Predicate = predicateName, Type = type, Value = value });
                               // Debug.Log("Predicate: " + predicateName + " type : " + type + " value : " + allPredicates[j][k]["value"].ToString());
                            }

                        }

                        //}
                        
                        if (check == 0)
                        {

                            _info.SetActive(true);
                            infoCanvasIsActive = true;

                            check = 1;
                            Cursor.lockState = CursorLockMode.None;
                            Cursor.visible = true;
                            //Time.timeScale = 0;
                            cameralook.enabled = false;
                            playerlook.enabled = false;
                        }
                    }
                }
                
            }
            
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    // Update is called once per frame
    void Update () {
	
	}

    void degradoInfo(string nodo)
    {
        try
        {
            //Effettuiamo la chiamata per ottenere la risposta JSON
            //Debug.Log("Url di partenza: "+url );
            nodo = nodo.Replace("\"", string.Empty);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://5.249.129.167:8080/middleware/nodeTreeById?node="+nodo);
            request.Method = "GET";
            request.ContentType = "application/json";

            //Recupero il Subject di interesse, passato nella Request
            var nodeParameter = request.RequestUri.Query.ToString();
            var subjectOfInterest = nodeParameter.Replace("?node=", string.Empty);
            //Debug.Log("degrado "+subjectOfInterest);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            var responseValue = string.Empty;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (var responseStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseValue = reader.ReadToEnd();


                        var deserializedResponse = JSONNode.Parse(responseValue);
                        var resultRdfJsonNodes = deserializedResponse["result"]; //tutto l'rdf (e SOLO l'RDF!!!!)
                        var allSubjects = (JSONClass)JSON.Parse(resultRdfJsonNodes.ToString()); //parsiamo i nodi dell'RDF iniziale
                        var subjectNode = allSubjects.GetNodesByKey(subjectOfInterest); //recupero solo il soggetto che mi interessa...
                        //Debug.Log(resultRdfJsonNodes);
                        //Partendo da ogni soggetto, ci consideriamo i predicati e poi cicliamo ulteriormente
                        //per gli oggetti...

                        var allPredicates = (JSONClass)subjectNode;
                        var predicateNames = allPredicates.GetDictKeys().ToList();

                        for (int j = 0; j < allPredicates.Count; j++)
                        {
                            //Recupero il nome del predicato j-mo
                            var predicateName = predicateNames[j];
                            //Per il predicato j-mo, mi vado a prendere tutti i figli (quindi gli oggetti) 
                            var allObjects = allPredicates[j].Childs.ToList();
                            //Ciclo per ogni oggetto e me lo vado ad aggiungere nella lista
                            for (int k = 0; k < allObjects.Count; k++)
                            {
                                if (predicateName.Contains("TIPODEG"))
                                {
                                    //add colonBio
                                    if(allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty)== "Alter_cromatica")
                                    {
                                        MATERIALI_DESC.text += "Alterazione cromatica \n";
                                    }
                                    else if (allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty) == "colonBio")
                                    {
                                        MATERIALI_DESC.text += "Colonizzazone biologica \n";
                                    }
                                    else if (allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty) == "Pat_biologica")
                                    {
                                        MATERIALI_DESC.text += "Patina biologica \n";
                                    }
                                    else if (allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty) == "Degr_differenziale")   
                                    {
                                        MATERIALI_DESC.text += "Degradazione differenziale \n";
                                    }
                                    else if (allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty) == "Dep_superficiale")
                                    {
                                        MATERIALI_DESC.text += "Deposito superficiale \n";
                                    }
                                    else
                                    {
                                        MATERIALI_DESC.text += allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty) + "\n";
                                    }
                                    
                                    //var type = allPredicates[j][k]["type"].ToString();
                                    //var value = allPredicates[j][k]["value"].ToString();
                                    //Debug.Log("2Predicate: " + predicateName + " type : " + type + " value : " + allPredicates[j][k]["value"].ToString());
                                }
                                
                                if (predicateName.Contains("ESTENSDEG"))
                                {
                                    MATERIALI_MQ.text += allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty)+"\n";
                                }
                                if (predicateName.Contains("PERCDEG"))
                                {
                                    MATERIALI_PERC.text += allPredicates[j][k]["value"].ToString().Replace("\"", string.Empty)+"\n";
                                }
                                
                                //var type = allPredicates[j][k]["type"].ToString();
                                //var value = allPredicates[j][k]["value"].ToString();
                                //rdfNodes.Add(new RDFData { Predicate = predicateName, Type = type, Value = value });
                                //Debug.Log("2Predicate: " + predicateName + " type : " + type + " value : " + allPredicates[j][k]["value"].ToString());
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }

    }
}
