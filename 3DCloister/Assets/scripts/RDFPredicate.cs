﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RDFExperiments
{
    public class RDFPredicate
    {
        public RDFPredicate()
        {
            RDFObjects = new List<RDFData>();
        }

        public string Predicate { get; set; }
        public List<RDFData> RDFObjects { get; set; }

    }
}