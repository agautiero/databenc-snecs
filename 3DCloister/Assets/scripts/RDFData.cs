﻿namespace RDFExperiments
{
    public class RDFData
    {
        public string Predicate { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
