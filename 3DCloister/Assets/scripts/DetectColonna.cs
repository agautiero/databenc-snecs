﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DetectColonna : MonoBehaviour {
	public Color colorCol;
	public AudioClip over;
	public AudioClip click;
	public GameObject _info;
	public MouseLook cameralook;
	public MouseLook playerlook;

	public Text materiale;
	public Text facciata;

	public int idColonna = 0;
	public Image infoIMG;

	public static bool infoCanvasIsActive;

	public static int check = 0;
	// Use this for initialization
	void Start () {
		//Canvas informazioni = GetComponent<Canvas> ();
		_info.SetActive(false);
		materiale.text = "";
		facciata.text = "";
	}
	public void OnMouseEnter(){
		if (!ShowPanels.optionPanelIsActive) {
			if (!_info.activeSelf){
				Renderer rend = GetComponent<Renderer> ();
				AudioSource suono = GetComponent<AudioSource> ();
				suono.PlayOneShot (over);
				//suono.Play(44100);
				//rend.material.color = Color.red;
				rend.material.color = colorCol;
				check = 0;
			}
		}
	}

	public void OnMouseExit(){
		Renderer rend = GetComponent<Renderer> ();
		rend.material.color = Color.white;

	}
    
	public void OnMouseDown(){
		
		//roba serissima, dapro su unity3d...controlla se il puntatore si trova su un evento
		if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
			if (!_info.activeSelf) {
				showImage (idColonna);
				AudioSource suono = GetComponent<AudioSource> ();
				suono.PlayOneShot (click);
				//Debug.Log ("Info Colonna");
				if (check == 0) {
			
					_info.SetActive (true);
					infoCanvasIsActive = true;
					check = 1;
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
					//Time.timeScale = 0;
					cameralook.enabled = false; 
					playerlook.enabled = false;
				}
			}
		}
	}


	public void showImage (int idColonna) {
		Debug.Log (idColonna);
		//if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
			if (!_info.activeSelf) {
			cameralook.enabled = false; 
			playerlook.enabled = false;
				infoIMG.overrideSprite = Resources.Load<Sprite> (idColonna + "");
				changeText(idColonna);
				AudioSource suono = GetComponent<AudioSource> ();
				suono.PlayOneShot (click);
				//Debug.Log ("Info Colonna");
				if (check == 0) {

					_info.SetActive (true);
					infoCanvasIsActive = true;
					
					check = 1;
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
					//Time.timeScale = 0;
					cameralook.enabled = false; 
					playerlook.enabled = false;
				}
			}
		//}
		//infoIMG.overrideSprite = Resources.Load<Sprite> (id + "");
	}
    
	public void changeText (int idColonna) {
		Debug.Log (idColonna);
		//if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) {
			switch (idColonna) {
			case 0:
			facciata.text = "Portali";
				materiale.text = "Piperno";
				break;
			case 1:
				facciata.text = "Portali";
				materiale.text = "Calcare/Marmo";
				break;
			case 2:
				facciata.text = "Portali";
				materiale.text = "Intonaco";
				break;
			case 3:
				facciata.text = "Biblioteca";
				materiale.text = "Piperno";
				break;
			case 4:
				facciata.text = "Biblioteca";
				materiale.text = "Intonaco";
				break;
			case 5:
				facciata.text = "Geologia";
				materiale.text = "Piperno";
				break;
			case 6:
				facciata.text = "Geologia";
				materiale.text = "Intonaco";
				break;
			case 7:
				facciata.text = "Chiesa";
				materiale.text = "Piperno";
				break;
			case 8:
				facciata.text = "Chiesa";
				materiale.text = "Intonaco";
				break;
			default: 
				break;
			}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
